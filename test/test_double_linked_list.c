#include "unity.h"
#include "double_linked_list.h"
#include "string.h"
#include <stdbool.h>


void setUp ( void )
{
}

void tearDown ( void )
{
}


void test_double_linked_list_create_should_create_pointer ( )
{
    /* Ensure known test state */
    int data[255];
    /* Setup expected call chain */
    /* Call function under test */
    element_double_linked_list *list = double_linked_list_create ( data );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( data, list->data );
}

void test_double_linked_list_add_element_shoud_add_elements ( )
{
    /* Ensure known test state */
    int data[255];

    element_double_linked_list *list = double_linked_list_create ( data );
    TEST_ASSERT_EQUAL ( data, list->data );


    /* Call function under test */
    double_linked_list_add_element ( list, data + 1 );
    TEST_ASSERT_EQUAL ( data + 1, list->next->data );
}

void test_double_linked_list_add_element_shoud_remove_elements_end_of_list ( )
{
    /* Ensure known test state */
    int *data[] = { malloc (1),malloc (1),malloc (1) };
    *data[0] = 1;
    *data[1] = 2;
    *data[2] = 3;


    element_double_linked_list *list = double_linked_list_create ( data[0] );
    TEST_ASSERT_EQUAL ( data[0], list->data );
    double_linked_list_add_element ( list, data[1] );
    TEST_ASSERT_EQUAL ( data[1], list->next->data );


    /* Call function under test */
    double_linked_list_delete_element ( list->next );
    TEST_ASSERT_EQUAL (NULL, list->next );
}

void test_double_linked_list_add_element_shoud_remove_one_element_with_elements_after ( )
{
    /* Ensure known test state */
    int *data[] = { malloc (1),malloc (1),malloc (1) };
    *data[0] = 1;
    *data[1] = 2;
    *data[2] = 3;

    element_double_linked_list *list = double_linked_list_create ( data[0] );
    TEST_ASSERT_EQUAL ( data[0], list->data );
    double_linked_list_add_element ( list, data[1] );
    TEST_ASSERT_EQUAL ( data[1], list->next->data );
    double_linked_list_add_element ( list->next, data[2]);
    TEST_ASSERT_EQUAL ( data[2], list->next->next->data );


    element_double_linked_list *tail = list->next->next;
    /* Call function under test */
    double_linked_list_delete_element ( list->next );
    TEST_ASSERT_EQUAL ( tail, list->next );
}

void test_double_linked_list_add_element_shoud_remove_first_element ( )
{
    /* Ensure known test state */
    int *data[] = { malloc (1),malloc (1),malloc (1) };
    *data[0] = 1;
    *data[1] = 2;
    *data[2] = 3;

    element_double_linked_list *list = double_linked_list_create ( data[0] );
    TEST_ASSERT_EQUAL ( data[0], list->data );
    double_linked_list_add_element ( list, data[1] );
    TEST_ASSERT_EQUAL ( data[1], list->next->data );

    element_double_linked_list *tail = list->next;
    /* Call function under test */
    double_linked_list_delete_element ( list );
    TEST_ASSERT_EQUAL ( NULL, tail->prev );
}


void test_double_linked_list_print_elements_shoud_print_forward ( )
{
    /* Ensure known test state */
    int *data[] = { malloc (1),malloc (1),malloc (1) };
    *data[0] = 1;
    *data[1] = 2;
    *data[2] = 3;

    element_double_linked_list *list = double_linked_list_create ( data[0] );
    TEST_ASSERT_EQUAL ( data[0], list->data );
    double_linked_list_add_element ( list, data[1] );
    TEST_ASSERT_EQUAL ( data[1], list->next->data );
    double_linked_list_add_element ( list->next, data[2] );
    TEST_ASSERT_EQUAL ( data[2], list->next->next->data );

    /* Call function under test */
    double_linked_list_print ( list, true );
}