#ifndef CPROJECT_DOUBLE_LINKED_LIST_DOUBLE_LINKED_LIST_H
#define CPROJECT_DOUBLE_LINKED_LIST_DOUBLE_LINKED_LIST_H

#include <stdbool.h>
#include <stddef.h>
#include <malloc.h>

/*! \mainpage CProject double_linked_list
 *
 * \section intro_sec Introduction
 *
 * This is a little cproject with unit-testing (ceedling) and documentation(doxygen). They are both launched by gitlab followed by the documentation deployment through Gitlab Pages.
 *
 * the main interface is <a href="double__linked__list_8h.html">here</a>
 *
 * <a href="https://gitlab.com/ThibF/cproject_double_linked_list">Gitlab project</a>
 *
 * \section sw_sec SW Assessment
 *
 *  Write a program that create a circular, doubled linked list, with the following functions:
 *
 *   - List allocation
 *
 *   - List deletion
 *
 *   - Add element
 *
 *   - Remove element
 *
 *   - Traverse list forward, printing all elements
 *
 *   - Traverse list backward, printing all elements
 *
 *   - Find a specific element and print it.
 *
 *   - Enough comments to explain functions functionality.
 *
 * Preference would be on:
 *
 *   - Programming language: C ANSI/C99/C11
 *
 *   - Build file included (Make, CMake, anything else...)
 *
 * \section compilation Compilation :
 *
 * make
 *
 * \section impro_sec Possible tooling improvement
 *
 * Generate test coverture.
 *
 * Put this section in a seperate file.
 *
 * Add a README.md.
 *
 * Add a Licence.
 */


/*! \file double_linked_list.h
    \brief Interface for double linked_list creation, insert, deletion.
*/

typedef struct element_double_linked_list
{
    struct element_double_linked_list *prev;
    struct element_double_linked_list *next;
    void *data;
} element_double_linked_list;


/*! \fn element_double_linked_list *double_linked_list_create ( void *data );
    \brief Create a initial element to a double linked list with \a data inside.
    \param data pointer to data to store in element, this element is not allocated and need to be allocated beforehand.
    \return pointer onto this double linked list element.
*/

element_double_linked_list *double_linked_list_create ( void *data );

/*! \fn void double_linked_list_delete ( element_double_linked_list *head );
    \brief Delete whole linked list, deallocate every data stored in it.
    \param head pointer onto the double linked list element to delete.
*/
void double_linked_list_delete ( element_double_linked_list *head );

/*! \fn void double_linked_list_add_element ( element_double_linked_list *current_element, void *data );
    \brief insert an element after \a current_element with data stored in it.
    \param current_element we insert the new data into the list after the \a current_element.
*/
void double_linked_list_add_element ( element_double_linked_list *current_element, void *data );

/*! \fn void double_linked_list_delete_element ( element_double_linked_list *current_element );
    \brief delete the \a current_element and deallocate data stored in it.
    \param current_element Element to delete.
*/
void double_linked_list_delete_element ( element_double_linked_list *current_element );

/*! \fn void double_linked_list_print ( element_double_linked_list *current_element, bool is_forward );
    \brief print data of every element included \a current_element in the way indicated by \a is_forward. Data stored is printed as int.
    \param current_element Element where to begin to print.
    \param is_forward Indicate if we print forward (true) or backward (false).
*/
void double_linked_list_print ( element_double_linked_list *current_element, bool is_forward );

/*! \fn void double_linked_list_print_index ( element_double_linked_list *current_element, int index );
    \brief print data with \a index relatively to current element. Data stored is printed as int.
    \param current_element Element where to begin to count.
    \param index positive mean forward, negative mean backward, 0 mean current element.
*/
void double_linked_list_print_index ( element_double_linked_list *current_element, int index );

#endif //CPROJECT_DOUBLE_LINKED_LIST_DOUBLE_LINKED_LIST_H
