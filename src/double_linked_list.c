#include "double_linked_list.h"

element_double_linked_list *double_linked_list_create ( void *data )
{
    element_double_linked_list *created_list;
    created_list = malloc ( sizeof ( element_double_linked_list ));
    created_list->next = NULL;
    created_list->prev = NULL;
    created_list->data = data;
    return created_list;
}

void double_linked_list_delete ( element_double_linked_list *head )
{
    if ( head == NULL )
    {
        return;
    }
    while ( head->prev != NULL)
    {
        double_linked_list_delete_element ( head->prev );
    }
    while ( head->next != NULL)
    {
        double_linked_list_delete_element ( head->next );
    }
    double_linked_list_delete_element ( head );
}

void double_linked_list_add_element ( element_double_linked_list *current_element, void *data )
{
    if ( current_element == NULL )
    {
        return;
    }
    element_double_linked_list *new_element = double_linked_list_create ( data );
    new_element->prev = current_element;
    new_element->next = current_element->next;
    current_element->next = new_element;
}

void double_linked_list_delete_element ( element_double_linked_list *current_element )
{
    if ( current_element == NULL )
    {
        return;
    }
    if ( current_element->prev != NULL)
    {
        current_element->prev->next = current_element->next;
    }
    if ( current_element->next != NULL)
    {
        current_element->next->prev = current_element->prev;
    }
    free ( current_element->data );
    free ( current_element );

}

void double_linked_list_print ( element_double_linked_list *current_element, bool is_forward )
{
    if ( current_element == NULL )
    {
        return;
    }
    element_double_linked_list *element_to_print = current_element;
    printf ( "%d ", *((int *) element_to_print->data ));
    if ( is_forward )
    {
        while ( element_to_print->next != NULL)
        {
            element_to_print = element_to_print->next;
            printf ( "%d ", *((int *) element_to_print->data ));
        }
    }
    else
    {
        while ( element_to_print->prev != NULL)
        {
            element_to_print = element_to_print->prev;
            printf ( "%d ", *((int *) element_to_print->data ));
        }
    }
}

void double_linked_list_print_index ( element_double_linked_list *current_element, int index )
{
    if ( current_element == NULL )
    {
        return;
    }
    if ( index >= 0 )
    {
        while ( index != 0 )
        {
            current_element = current_element->next;
            index--;
        }
        printf ( "%d ", *((int *) current_element->data ));
    }
    else
    {
        while ( index != 0 )
        {
            current_element = current_element->prev;
            index++;
        }
        printf ( "%d ", *((int *) current_element->data ));
    }
}
