#include <stdio.h>
#include "../src/double_linked_list.h"

int main ( )
{
    int *data[] = { malloc (1),malloc (1),malloc (1) };
    *data[0] = 1;
    *data[1] = 2;
    *data[2] = 3;

    element_double_linked_list *list = double_linked_list_create ( data[0] );
    double_linked_list_add_element ( list, data[1] );
    double_linked_list_add_element ( list->next, data[2] );

    /* Call function under test */
    double_linked_list_print ( list, true );
    double_linked_list_print ( list->next->next, false );
    printf("\n");
    double_linked_list_print_index ( list, 0 );
    double_linked_list_print_index ( list, 1 );
    double_linked_list_print_index ( list, 2 );
    double_linked_list_print_index ( list->next, -1 );
    printf("\n");

    double_linked_list_delete_element ( list->next );
    double_linked_list_print ( list, true );
    return 0;
}